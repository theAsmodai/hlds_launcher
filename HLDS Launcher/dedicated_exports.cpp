#include "precompiled.h"

class CDedicatedExports : IDedicatedExports
{
public:
	virtual void Sys_Printf(char *text)
	{
		char cmd[256];
		size_t len = fread(cmd, 1, sizeof cmd, stdin);
		printf("%s", text);
		fflush(stdout);
		fwrite(cmd, 1, len, stdin);
	}
};

EXPOSE_SINGLE_INTERFACE(CDedicatedExports, IDedicatedExports, VENGINE_DEDICATEDEXPORTS_API_VERSION);
