#pragma once

#ifdef _WIN32
typedef void* CSysThread;
#define THREAD_DECL __stdcall
#else
typedef long CSysThread;
#define THREAD_DECL
#endif

enum pingboost_e
{
	pb_disabled,
	pb_valve,
	pb_new,
	pb_valve3,
	pb_extreme
};

enum fpsboost_e
{
	fb_disabled,
	fb_std,
	fb_system,
	fb_precise,
	fb_float
};

class CSystem
{
public:
	CSystem(CCmdLine* cmdline, CSysModule* engine);
	~CSystem();

	int getPingBoost() const;
	int getFpsBoost() const;
	void USleep(unsigned int usec);
	void USleep_Thread(unsigned int usec);
	CSysModule* GetEngine() const;
	static CSysThread RunThread(void* func, void* arg);
	static void JoinThread(CSysThread thread);
	double NOINLINE FloatTime() const;
	bool WaitForInput(unsigned int timeout_msec) const;
	static void __declspec(noreturn) CriticalError(const char* fmt, ...);

private:
	void (CSystem::*Sys_USleep)(unsigned int usec);
	void (*NET_Sleep_Timeout)();
	int m_ServerSocket;
	CSysModule* m_EngineModule;
	int m_pingboost;
	int m_fpsboost;

	void USleep_STD(unsigned int usec);
	void USleep_Valve(unsigned int usec);
	void USleep_Engine_PB(unsigned int usec);

#ifdef _WIN32
	void USleep_NT_Main(unsigned int usec);
	void USleep_NT(unsigned int usec);
	void USleep_PB_Main(unsigned int usec);
	void USleep_PB(unsigned int usec);

	NTSTATUS (__stdcall *NtDelayExecution)(IN BOOL Alertable, IN PLARGE_INTEGER DelayInterval);
	
	HANDLE m_eventSockRead;
	double m_TimerFrequency;
	LARGE_INTEGER m_TimerStartTime;
	HANDLE m_stdinHandle;
#else
	struct timespec m_TimerStartTime;
	pollfd m_cinfd
	int m_epfd;
#endif
};
