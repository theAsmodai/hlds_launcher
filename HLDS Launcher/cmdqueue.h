#pragma once

struct qcmd
{
	char text[128];
	qcmd* next;
	int executed;
};

class CCmdQueue
{
public:
	CCmdQueue();
	~CCmdQueue();

	void addCmd(const char* text);
	qcmd* getCmd() const;
	void clear();

private:
	void checkExecuted();
	void writeBegin();
	void writeEnd();
	qcmd* readHead() const;
	qcmd* readNext(qcmd* cmd) const;

	qcmd* m_head;
	volatile size_t m_sequence;
};
