#pragma once

#define MAX_CMDLINE	2048

class CCmdLine
{
public:
	CCmdLine(const char** argv, int argc);

	char*			getString();
	int				getParmInt(const char* name, int errval = -1) const;
	bool			getParmBool(const char* name) const;
	const char*		getParmString(const char* name, const char* errval = 0) const;

private:
	int				findParm(const char* name) const;

	char			m_string[MAX_CMDLINE];
	const char**	m_argv;
	int				m_argc;
};