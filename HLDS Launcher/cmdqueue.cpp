#include "precompiled.h"

CCmdQueue::CCmdQueue() : m_head(NULL), m_sequence(0)
{

}

CCmdQueue::~CCmdQueue()
{
	clear();
}

void CCmdQueue::addCmd(const char* text)
{
	checkExecuted();

	qcmd* cmd = new qcmd;
	strncpy(cmd->text, text, sizeof cmd->text - 1);
	cmd->text[sizeof cmd->text - 1] = '\0';
	cmd->next = NULL;
	cmd->executed = 0;

	qcmd* last = m_head;
	if (last)
		while (last->next)
			last = last->next;

	qcmd** dest = &(last ? last->next : m_head);
	writeBegin();
	*dest = cmd;
	writeEnd();
}

qcmd* CCmdQueue::getCmd() const
{
	for (qcmd* cmd = readHead(); cmd != NULL; cmd = readNext(cmd))
		if (!cmd->executed)
			return cmd;
	return NULL;
}

void CCmdQueue::clear()
{
	for (qcmd* cmd = m_head; cmd != NULL;)
	{
		auto ptr = cmd;
		cmd = cmd->next;
		delete ptr;
	}

	m_head = NULL;
}

void CCmdQueue::checkExecuted()
{
	qcmd* prev = NULL;

	for (qcmd* cmd = m_head; cmd != NULL; cmd = cmd->next)
	{
		if (cmd->executed)
		{
			qcmd** dest = &(prev ? prev->next : m_head);
			writeBegin();
			*dest = cmd->next;
			writeEnd();

			delete cmd;
			checkExecuted();
			break;
		}

		prev = cmd;
	}
}

void CCmdQueue::writeBegin()
{
	m_sequence++;
}

void CCmdQueue::writeEnd()
{
	m_sequence++;
}

qcmd* CCmdQueue::readHead() const
{
	size_t seq1 = m_sequence;
	qcmd* head = m_head;
	size_t seq2 = m_sequence;
	__asm nop;
	return (seq1 == seq2) ? head : NULL;
}

qcmd* CCmdQueue::readNext(qcmd* cmd) const
{
	size_t seq1 = m_sequence;
	qcmd* next = cmd->next;
	size_t seq2 = m_sequence;
	__asm nop;
	return (seq1 == seq2) ? next : NULL;
}
