#include "precompiled.h"

int CSystem::getPingBoost() const
{
	return m_pingboost;
}

int CSystem::getFpsBoost() const
{
	return m_fpsboost;
}

#ifdef _WIN32
void CSystem::USleep_NT_Main(unsigned int usec)
{
	LARGE_INTEGER interval;
	interval.QuadPart = -(int)(usec * 10);
	NtDelayExecution(FALSE, &interval);
}

void CSystem::USleep_NT(unsigned int usec)
{
	HMODULE ntdll = GetModuleHandle("ntdll.dll");
	NtDelayExecution = (NTSTATUS(__stdcall *)(BOOL, PLARGE_INTEGER))GetProcAddress(ntdll, "NtDelayExecution");
	auto ZwSetTimerResolution = (NTSTATUS(__stdcall *)(ULONG, BOOLEAN, PULONG))GetProcAddress(ntdll, "ZwSetTimerResolution");

	ULONG actualResolution;
	ZwSetTimerResolution(500, TRUE, &actualResolution); // n * 100nsec

	Sys_USleep = &CSystem::USleep_NT_Main;
	USleep_NT_Main(usec);
}

void CSystem::USleep_Thread(unsigned int usec)
{
	SwitchToThread();
}

void CSystem::USleep_PB_Main(unsigned int usec)
{
	auto timeout = (usec < 1000) ? 1 : usec / 1000;
	DWORD dwRet = WaitForSingleObject(m_eventSockRead, timeout);
}

void CSystem::USleep_PB(unsigned int usec)
{
	m_eventSockRead = WSACreateEvent();
	WSAEventSelect(m_ServerSocket, m_eventSockRead, FD_READ);
	Sys_USleep = &CSystem::USleep_PB_Main;
	USleep_PB_Main(usec);
}
#else
void CSystem::USleep_PB_Main(unsigned int usec)
{
	struct epoll_event events;
	epoll_wait(m_epfd, &events, 1, -1);
}

void CSystem::USleep_PB(unsigned int usec)
{
	if ((m_epfd = epoll_create1(EPOLL_CLOEXEC)) == -1) {
		CriticalError("epoll_create1 failed");
	}

	struct epoll_event ev;
	ev.data.fd = m_epfd;
	ev.events = EPOLLIN;
	ev.data.ptr = NULL;

	epoll_ctl(m_epfd, EPOLL_CTL_ADD, m_ServerSocket, &ev);

	Sys_USleep = &CSystem::USleep_PB_Main;
	USleep_PB_Main(usec);
}

void CSystem::USleep_Thread(unsigned int usec)
{
	pthread_yield();
}
#endif

void CSystem::USleep_STD(unsigned int usec)
{
#ifdef _WIN32
	std::this_thread::sleep_for(std::chrono::microseconds(usec));
#else
	usleep(usec);
#endif
}

void CSystem::USleep_Valve(unsigned int usec)
{
#ifdef _WIN32
	Sleep(1);
#else
	usleep(1000);
#endif
}

void CSystem::USleep_Engine_PB(unsigned int usec)
{
	NET_Sleep_Timeout();
}

CSystem::CSystem(class CCmdLine* cmdline, CSysModule* engine) : m_ServerSocket(INVALID_SOCKET), m_EngineModule(engine)
{
#ifdef _WIN32
	// Initialize Winsock
	WSADATA wsaData;

	if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
		CriticalError("WSAStartup failed.");

	// Initialize timer
	LARGE_INTEGER PCFreq;
	QueryPerformanceFrequency(&PCFreq);
	m_TimerFrequency = double(PCFreq.QuadPart);
	QueryPerformanceCounter(&m_TimerStartTime);

	// Initialize console
	m_stdinHandle = GetStdHandle(STD_INPUT_HANDLE);

	m_eventSockRead = NULL;
	NtDelayExecution = NULL;
#else
	const char* pidfile = cmdline.getParmString("-pidfile");

	if (pidfile) {
		fp = fopen(pidfile, "w");

		if (fp) {
			fprintf(fp, "%i\n", getpid());
			fclose(fp);
		}
		else
			printf("Warning: unable to open pidfile (%s)\n", pidfile);
	}

	// Initialize timer
	clock_gettime(1, &m_TimerStartTime);

	// Initialize console
	pollfd fd = {fileno(stdin), POLLIN, 0};
	m_cinfd = fd;
#endif

	Reunion_RehldsApi_Init(engine);

	if (g_RehldsFuncs) {
		m_ServerSocket = g_RehldsFuncs->GetServerSocket(NS_SERVER);
	}

	Sys_USleep = NULL;
	NET_Sleep_Timeout = (void(*)())Sys_GetProcAddress(engine, "NET_Sleep_Timeout");

	m_pingboost = cmdline->getParmInt("-pingboost", pb_disabled);
	m_fpsboost = cmdline->getParmInt("-fpsboost", fb_disabled);

	switch (m_pingboost)
	{
	case pb_valve3:
		m_pingboost = pb_valve;
	case pb_valve:
		if (NET_Sleep_Timeout)
			Sys_USleep = &CSystem::USleep_Engine_PB;
		break;

	case pb_new:
	case pb_extreme:
		if (m_ServerSocket != INVALID_SOCKET)
			Sys_USleep = &CSystem::USleep_PB;
		break;

	default:
		;	
	}

	if (m_pingboost && !Sys_USleep) {
		m_pingboost = pb_disabled;
		printf("Warning: failed to initialize pingboost %i\n", m_pingboost);
	}

	if (m_pingboost == pb_extreme && m_fpsboost == fb_precise) {
		m_pingboost = pb_new;
		printf("Warning: -fpsboost %i doesn't compatible without -fpsboost %i.\n", pb_extreme, fb_precise);
	}

	switch (m_fpsboost)
	{
	case fb_std:
	case fb_float:
		if (!Sys_USleep)
			Sys_USleep = &CSystem::USleep_STD;
		break;

	case fb_system:
		m_pingboost = pb_disabled;
#ifdef _WIN32
		Sys_USleep = &CSystem::USleep_NT;
#else
		Sys_USleep = &CSystem::USleep_STD;
#endif
		break;

	case fb_precise:
		if (!Sys_USleep || m_pingboost == pb_valve)
#ifdef _WIN32
			Sys_USleep = &CSystem::USleep_NT;
#else
			Sys_USleep = &CSystem::USleep_STD;
#endif
		break;

	default:
		if (!Sys_USleep)
			Sys_USleep = &CSystem::USleep_Valve;
	}
}

CSystem::~CSystem()
{
#ifdef _WIN32
	WSACloseEvent(m_eventSockRead);
#endif
}

void CSystem::USleep(unsigned usec)
{
	return (this->*Sys_USleep)(usec);
}

CSysModule* CSystem::GetEngine() const
{
	return m_EngineModule;
}

CSysThread CSystem::RunThread(void* func, void* arg)
{
	CSysThread thread;
#ifdef _WIN32
	thread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)func, arg, 0, NULL);
#else
	pthread_create(&thread, NULL, (void *(*)(void *))func, arg);
#endif
	return thread;
}

void CSystem::JoinThread(CSysThread thread)
{
#ifdef WIN32
	WaitForSingleObject(thread, INFINITE);
#else
	pthread_join(thread, nullptr);
#endif
}

void __declspec(noreturn) CSystem::CriticalError(const char* fmt, ...)
{
	va_list argptr;
	char string[8192];

	va_start(argptr, fmt);
	vsnprintf(string, sizeof string, fmt, argptr);
	va_end(argptr);

	puts(string);
	exit(-1);
}

#ifdef _WIN32
double NOINLINE CSystem::FloatTime() const
{
	LARGE_INTEGER PerformanceCount;
	QueryPerformanceCounter(&PerformanceCount);
	return double(PerformanceCount.QuadPart - m_TimerStartTime.QuadPart) / m_TimerFrequency;
}
#else
double NOINLINE CSystem::FloatTime()
{
	struct timespec now;
	clock_gettime(1, &now);
	return (now.tv_sec - m_TimerStartTime.tv_sec) + now.tv_nsec * 0.000000001;
}
#endif

#ifdef _WIN32
bool CSystem::WaitForInput(unsigned int timeout_msec) const
{
	return WaitForSingleObject(m_stdinHandle, timeout_msec) == WAIT_OBJECT_0;
}
#else
bool CSystem::WaitForInput(unsigned int timeout_msec)
{
	return poll(&m_cinfd, 1, timeout_msec) > 0;
}
#endif
