#pragma once

#ifdef WIN32
#include <winsock2.h>
#include <windows.h>
#include <chrono>
#include <thread>
#else
#include <unistd.h>
#include <sys/epoll.h>
#endif

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <varargs.h>
#include <stddef.h>

#include <mathlib.h>
#include <eiface.h>
#include <rehlds_api.h>
#include <FileSystem.h>
#include <engine_hlds_api.h>
#include <dll_state.h>
#include <idedicatedexports.h>

#ifdef WIN32
#define NOINLINE __declspec(noinline)
#else
#define NOINLINE __attribute__((noinline))
#define INVALID_SOCKET	-1
#endif

#include "cmdline.h"
#include "interface.h"
#include "launcher_const.h"
#include "cmdqueue.h"
#include "system.h"
#include "launcher_rehlds_api.h"
