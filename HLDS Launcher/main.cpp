#include "precompiled.h"

struct thread_data_t
{
	int exit;
	IDedicatedServerAPI* engineAPI;
	CCmdQueue cmds;
	CCmdLine* cmdline;
	CSystem* system;
};

qboolean AllowRunFrame_Default(IRehldsHook_AllowRun_ServerFrame* chain, float desired_frame_time, float current_frame_time)
{
	return desired_frame_time <= current_frame_time;
};

qboolean AllowRunFrame_AlwaysTrue(IRehldsHook_AllowRun_ServerFrame* chain, float desired_frame_time, float current_frame_time)
{
	return TRUE;
};

int THREAD_DECL EngineThread(void* arg)
{
	thread_data_t* td = (thread_data_t *)arg;
	auto engine = td->engineAPI;
	auto system = td->system;

	qcmd* cmd = nullptr;
	bool disable_engine_fps_limiter = system->getPingBoost() == pb_extreme;

	int ticrate = td->cmdline->getParmInt("-sys_ticrate", 0);
	int fpsboost = system->getFpsBoost();

	if (fpsboost && !ticrate) {
		puts("Warning: -fpsboost doesn't work without -sys_ticrate launch option.");
		fpsboost = fb_disabled;
	}

	const double MICROSECONDS_IN_SECOND = 1000000.0;

	enum sleep_mode {
		sm_static,
		sm_dynamic,
		sm_step
	};

	double frame_time			= 1.0 / double(ticrate + 1);
	unsigned int sleep_time		= unsigned(frame_time * MICROSECONDS_IN_SECOND / 10.0);
	unsigned int sleep_step		= sleep_time;
	sleep_mode sleep_mode		= sm_static;
	bool always_sleep			= true;

	switch (fpsboost) {
	case fb_std:
		sleep_mode = sm_dynamic;
		break;

	case fb_system:
		sleep_time = unsigned(frame_time * MICROSECONDS_IN_SECOND);
		break; 

	case fb_precise:
		sleep_mode = sm_step;
		sleep_time = 1;
		break;

	case fb_float:
		sleep_time = unsigned(frame_time * MICROSECONDS_IN_SECOND);
		sleep_mode = sm_dynamic;
		disable_engine_fps_limiter = true;
		break;

	default: // valve
		sleep_time = 1000;
		sleep_step = 1000;
	}

	if (g_RehldsHookchains) {
		g_RehldsHookchains->AllowRun_ServerFrame()->registerHook(disable_engine_fps_limiter ? AllowRunFrame_AlwaysTrue : AllowRunFrame_Default);
	}

	double frameStart = 0.0;

	do {
		int remaining;

		switch (sleep_mode) {
		case sm_static:
			system->USleep(sleep_time);
			break;

		case sm_dynamic:
			remaining = int(double(sleep_time) - (system->FloatTime() - frameStart));
			if (remaining <= 0) {
				if (!always_sleep) {
					break;
				}
				remaining = 1;
			}
			system->USleep(remaining);
			break;

		case sm_step:
			if (always_sleep) {
				system->USleep(sleep_step);
			}
			while (system->FloatTime() - frameStart <= frame_time) {
				system->USleep(sleep_step);
			}
			break;
		}

		if ((cmd = td->cmds.getCmd()) != NULL) {
			engine->AddConsoleText(cmd->text);
			cmd->executed = TRUE;
		}

		frameStart = system->FloatTime();

	} while (engine->RunFrame());

	td->exit = TRUE;
	return 0;
}

int RunServer(CSystem* system, CCmdLine* cmdline, CreateInterfaceFn filesystemFactory)
{
	CreateInterfaceFn engineFactory = Sys_GetFactory(system->GetEngine());
	IDedicatedServerAPI* engineAPI;

	if (!engineFactory || ((engineAPI = (IDedicatedServerAPI *)engineFactory(VENGINE_HLDS_API_VERSION, NULL)) == NULL)) {
		puts("Can't get engine API.");
		return -1;
	}

	CSysThread thread;
	int state = DLL_INACTIVE;

	do {
		if (!engineAPI->Init(".", cmdline->getString(), Sys_GetFactoryThis(), filesystemFactory)) {
			puts("Can't init engine.");
			return -1;
		}

		thread_data_t td;
		td.exit = 0;
		td.engineAPI = engineAPI;
		td.cmdline = cmdline;
		td.system = system;

		thread = system->RunThread((void *)EngineThread, (void *)&td);

		do {
			char cmd[256];

			if (system->WaitForInput(CONSOLE_INPUT_TIMEOUT)) {
				fgets(cmd, sizeof cmd, stdin);
				td.cmds.addCmd(cmd);
				if (!strncmp(cmd, "_restart", 7))
					__asm int 3;
			}
		} while (!td.exit);
		
		state = engineAPI->Shutdown();

	} while (0);//(state != DLL_CLOSE);

	system->JoinThread(thread);
	return state;
}

int main(int argc, const char **argv)
{
	CCmdLine cmdline(argv, argc);

	printf("tick %i, ping %i, fps %i\n", cmdline.getParmInt("-sys_ticrate"), cmdline.getParmInt("-pingboost"), cmdline.getParmInt("-fpsboost"));
	getchar();

	int exit_code = DLL_INACTIVE;

	do {
		// Load engine
		CSysModule* engineModule = Sys_LoadModule(cmdline.getParmString("-binary", ENGINE_LIB));

		if (engineModule == NULL)
			CSystem::CriticalError("Unable to load engine, image is corrupt.");

		CSystem system(&cmdline, engineModule);

		// Load filesystem
		CSysModule* filesystemModule = Sys_LoadModule(FILESYSTEM_LIB);

		if (filesystemModule == NULL)
			CSystem::CriticalError("Unable to load filesystem, image is corrupt.");

		// Get FS interface
		CreateInterfaceFn filesystemFactory = Sys_GetFactory(filesystemModule);
		IFileSystem* pFileSystem;

		if (!filesystemFactory || ((pFileSystem = (IFileSystem *)filesystemFactory(FILESYSTEM_INTERFACE_VERSION, NULL)) == NULL))
			CSystem::CriticalError("Unable to init filesystem.");

		// Run
		pFileSystem->Mount();
		exit_code = RunServer(&system, &cmdline, filesystemFactory);
		pFileSystem->Unmount();

		// Unload
		Sys_UnloadModule(engineModule);
		Sys_UnloadModule(filesystemModule);

	} while (exit_code != DLL_CLOSE);

	getchar();
	return 0;
}
