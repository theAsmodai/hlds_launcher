#include "precompiled.h"

CCmdLine::CCmdLine(const char** argv, int argc) : m_argv(argv), m_argc(argc)
{
	if (argc <= 1)
		m_string[0] = '\0';
	else
	{
		int i, len = 0;

		for (i = 1; i < argc - 1; i++)
		{
			len += snprintf(m_string + len, sizeof m_string - len, "%s ", argv[i]);
		}

		snprintf(m_string + len, sizeof m_string - len, "%s", argv[i]);
	}
}

char* CCmdLine::getString()
{
	return m_string;
}

int CCmdLine::getParmInt(const char* name, int errval) const
{
	int id = findParm(name);

	if (!id || id == m_argc - 1)
		return errval;

	return atoi(m_argv[id + 1]);
}

bool CCmdLine::getParmBool(const char* name) const
{
	return getParmInt(name, 0) != 0;
}

const char* CCmdLine::getParmString(const char* name, const char* errval) const
{
	int id = findParm(name);

	if (!id || id == m_argc - 1)
		return errval;

	return m_argv[id + 1];
}

int CCmdLine::findParm(const char* name) const
{
	for (int i = 1; i < m_argc; i++)
	{
		if (!strcmp(m_argv[i], name))
			return i;
	}

	return 0;
}
