#pragma once

#define CONSOLE_INPUT_TIMEOUT	100

#ifdef _WIN32
#define ENGINE_LIB		"swds.dll"
#define FILESYSTEM_LIB	"FileSystem_Stdio.dll"

#define snprintf _snprintf
#else
#define ENGINE_LIB		"engine_i486.so"
#define FILESYSTEM_LIB	"filesystem_stdio.so"
#endif
